/**
 * @return {{isValid: boolean}}
 */

export function InputValidator(rules, value, key) {
  // Required Validation over a key
  if (rules.required) {
    let isValid;
    isValid = value.trim() !== "";
    if (!isValid) {
      return { isValid: isValid, helperText: `${key} is a required field.` };
    }
  }

  // MinLength Validation
  if (rules.minlength) {
    if (value.length < rules.minlength) {
      return {
        isValid: false,
        helperText: `${key} should have length greater than ${rules.minlength}.`
      };
    }
  }

  // Maxlength Validation
  if (rules.maxlength) {
    if (value.length > rules.maxlength) {
      return {
        isValid: false,
        helperText: `${key} should have length lesser than ${rules.maxlength}.`
      };
    }
  }

  // length Validation
  if (rules.length) {
    if (value.length !== rules["length"]) {
      return {
        isValid: false,
        helperText: `${key} should be ${rules["length"]} characters.`
      };
    }
  }

  if (rules.phoneNumber) {
    const Contact_REGEXP = new RegExp(/^[+\-0-9]{9,}$/);
    if (!Contact_REGEXP.test(value)) {
      return { isValid: false, helperText: `${key} is invalid.` };
    }
  }

  if (rules.email) {
    // eslint-disable-next-line
    const EMAIL_REGEXP = new RegExp(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]{2,}([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i);
    if (!EMAIL_REGEXP.test(value.trim())) {
      return { isValid: false, helperText: `${key} is invalid.` };
    }
  }

  return { isValid: true, helperText: "" };
}

export function formValidator(formObject) {
  let isValid = true;
  for (const key of Object.keys(formObject)) {
    if (key) {
      const formFieldState = InputValidator(
        formObject[key].validators,
        formObject[key].value,
        formObject[key].title
      );
      if (!formFieldState.isValid) {
        isValid = false;
      }
      formObject[key].success = formFieldState.isValid;
      formObject[key].error = !formFieldState.isValid;
      formObject[key].helpText = formFieldState.helperText;
    }
  }
  return isValid;
}
