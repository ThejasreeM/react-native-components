import React from "react";
import { View, Text, Image } from "react-native";
import { styles as cardStyles } from "./stylesheet";
import PropTypes from "prop-types";
import WLLogo from "../../UI/WLLogo";
import { CheckBox } from "react-native-elements";

const WLCard = props => {
  return (
    <View style={cardStyles.cardView}>
      <WLLogo
        source={require("../../assets/images/image1.jpeg")}
        style={cardStyles.image}
      />
      <View style={cardStyles.centeredTextView}>
        <Text style={cardStyles.mainText}>Amul toned milk pouch</Text>
        <Text style={cardStyles.text}>500ml</Text>
      </View>
      <View style={cardStyles.itemCountView}>
        <Text style={cardStyles.itemsCount}>12</Text>
        <Text style={cardStyles.itemsText}>items</Text>
      </View>
      <View style={cardStyles.checkedView}>
        <CheckBox
          checkedIcon={
            <Image
              source={require("../../assets/images/checked.png")}
              style={cardStyles.checkImage}
            />
          }
          uncheckedIcon={
            <Image
              source={require("../../assets/images/unchecked.png")}
              style={cardStyles.checkImage}
            />
          }
          checked={props.checked}
          onPress={() => this.setState({ checked: !props.checked })}
        />
      </View>
    </View>
  );
};

export default WLCard;

WLCard.propTypes = {
  navigation: PropTypes.object,
  checked: PropTypes.bool
};
