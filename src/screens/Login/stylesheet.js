/* eslint-disable react-native/sort-styles */
/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  // eslint-disable-next-line react-native/no-color-literals
  container: {
    backgroundColor: "grey",
    flex: 1
  },
  text: {
    alignSelf: "center",
    color: "#FFF",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 5
  }
});
