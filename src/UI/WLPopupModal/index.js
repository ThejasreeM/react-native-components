import React from "react";
import Modal from "react-native-modal";
import PropTypes from "prop-types";
import styles from "./stylesheet";

const WLPopupModal = props => {
  return (
    <Modal style={[styles.modal, props.style]} {...props}>
      {props.children}
    </Modal>
  );
};

WLPopupModal.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};

export default WLPopupModal;
