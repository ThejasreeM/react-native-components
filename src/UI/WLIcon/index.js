import React from "react";
import styles from "./stylesheet";
import { Icon } from "native-base";
import PropTypes from "prop-types";

const WLIcon = props => {
  return (
    <Icon name={props.name} style={[styles.icon, props.style]} {...props} />
  );
};

WLIcon.propTypes = {
  name: PropTypes.string,
  style: PropTypes.object
};

export default WLIcon;
