import React from "react";
import { Image } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

const WLLogo = props => {
  return <Image style={[styles.img, props.style]} {...props} />;
};

WLLogo.propTypes = {
  style: PropTypes.object
};

export default WLLogo;
