import Permissions from "react-native-permissions";
//Refer here for list of permissions
//https://github.com/yonahforst/react-native-permissions
export function checkPermissionStatus(permission) {
  return new Promise((resolve, reject) => {
    Permissions.check(permission)
      .then(res => {
        // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        switch (res) {
          case "authorized":
            resolve({ status: true, result: res });
            break;
          case "denied":
            requestPermission(permission)
              .then(res => resolve(res))
              .catch(err => reject(err));
            break;
          case "restricted":
            requestPermission(permission)
              .then(res => resolve(res))
              .catch(err => reject(err));
            break;
          case "undetermined":
            requestPermission(permission)
              .then(res => resolve(res))
              .catch(err => reject(err));
            break;
          default:
            break;
        }
      })
      .catch(err => reject(err));
  });
}

export function requestPermission(permission) {
  return new Promise((resolve, reject) => {
    Permissions.request(permission)
      .then(res => {
        switch (res) {
          case "authorized":
            resolve({ status: true, result: res });
            break;
          case "denied":
            resolve({ status: false, result: res });
            break;
          case "restricted":
            resolve({ status: false, result: res });
            break;
          case "undetermined":
            resolve({ status: false, result: res });
            break;
        }
      })
      .catch(err => reject(err));
  });
}
