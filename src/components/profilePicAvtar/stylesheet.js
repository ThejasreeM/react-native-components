import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  camView: {
    height: 100,
    width: 80
  },
  icon: {
    bottom: 40,
    left: 87,
    right: 0,
    top: 40
  },
  // eslint-disable-next-line react-native/no-color-literals
  imageView: {
    alignSelf: "center",
    backgroundColor: "grey",
    borderRadius: 50,
    height: 100,
    position: "absolute",
    width: 100
  }
});
