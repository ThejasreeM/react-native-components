import React from "react";
import { styles as cardStyles } from "./stylesheet";
import { View, Text, TouchableOpacity } from "react-native";
import WLIcon from "../../UI/WLIcon";
import WLLogo from "../..//UI/WLLogo";
import PropTypes from "prop-types";

const WLMainCard = props => {
  const onPressIcon = () => {
    props.navigation.navigate("CardList");
  };
  return (
    <View style={cardStyles.container}>
      <View style={cardStyles.card}>
        <WLLogo
          source={require("../../assets/images/image1.jpeg")}
          style={cardStyles.image}
        />
        <View style={cardStyles.centeredTextView}>
          <Text style={cardStyles.mainText}>Pick Orders</Text>
          <Text style={cardStyles.text}>from inventory</Text>
        </View>
        <TouchableOpacity onPress={onPressIcon}>
          <View style={cardStyles.iconView}>
            <WLIcon name="ios-arrow-forward" style={cardStyles.icon} />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WLMainCard;
WLMainCard.propTypes = {
  navigation: PropTypes.object
};
