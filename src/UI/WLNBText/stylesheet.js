import { StyleSheet } from "react-native";
import { TEXT_COLOR } from "../../../colors";

const styles = StyleSheet.create({
  text: {
    color: TEXT_COLOR,
    fontFamily: "Roboto",
    fontSize: 12
  }
});

export default styles;
