/* eslint-disable react/prop-types */
/* eslint-disable react-native/no-raw-text */
import React, { Component } from "react";
import { Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { styles as homeStyles } from "./stylesheet";
// import ProfileAvtar from "../../components/profilePicAvtar";
// import { ButtonWithBackground } from "../../components/ButtonWithBackground";
// import { BUTTON_BORDER_COLOR } from "../../../colors";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import WLButton from "../../UI/WLButton";
import WLText from "../../UI/WLText";
import alert from "../../components/Alert";
import RNSampleModal from "../../components/RNSampleModal";

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: null,
      showOTPModal: false
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Home",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      },
      headerLeft: (
        <Icon
          name={"arrow-left"}
          size={24}
          style={homeStyles.header}
          onPress={() => {
            navigation.navigate("Register");
          }}
        />
      )
    };
  };
  navigateToCart = () => {
    // eslint-disable-next-line react/prop-types
    this.props.navigation.navigate("Login");
  };
  checkAlert = () => {
    let title = "Sample Alert";
    alert(title);
  };
  checkFooter = () => {
    this.props.navigation.navigate("Cart");
  };
  checkCard = () => {
    this.props.navigation.navigate("Card");
  };
  checkMaps = () => {
    this.props.navigation.navigate("Orders");
  };
  checkPic = () => {
    this.props.navigation.navigate("Profile");
  };
  checkSlider = () => {
    this.props.navigation.navigate("Slider");
  };
  checkScanner = () => {
    this.props.navigation.navigate("Scanner");
  };
  checkCamScanner = () => {
    this.props.navigation.navigate("Dial");
  };
  render() {
    return (
      <KeyboardAwareScrollView
        style={homeStyles.container}
        scrollEnabled={true}
      >
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkAlert}>
          <WLText>
            <Text>ALERT</Text>
          </WLText>
        </WLButton>
        <WLButton
          style={homeStyles.btnSignUp}
          onPress={() => this.setState({ showOTPModal: true })}
        >
          <WLText>
            <Text>MODAL</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkFooter}>
          <WLText>
            <Text>FOOTER</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkCard}>
          <WLText>
            <Text>CARD</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkMaps}>
          <WLText>
            <Text>MAPS</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkPic}>
          <WLText>
            <Text>UPLOAD PIC</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkSlider}>
          <WLText>
            <Text>SLIDER</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkScanner}>
          <WLText>
            <Text>QR SCANNER</Text>
          </WLText>
        </WLButton>
        <WLButton style={homeStyles.btnSignUp} onPress={this.checkCamScanner}>
          <WLText>
            <Text>CALL</Text>
          </WLText>
        </WLButton>

        {/* <WLMainCard navigation={this.props.navigation} /> */}

        {/* <View>
          <TextInput
            style={homeStyles.textInput}
            placeholder="Enter code"
            onChangeText={code => this.setState({ code })}
            value={this.state.code}
            editable={true}
            maxLength={4}
            keyboardType={"default"}
            underlineColorAndroid={"transparent"}
          />
          <ButtonWithBackground
            color={BUTTON_BORDER_COLOR}
            style={homeStyles.textInputButton}
          >
            Validate
          </ButtonWithBackground>
        </View> */}

        {/* <ButtonWithBackground
          color={BUTTON_BORDER_COLOR}
          style={homeStyles.button}
          onPress={this.navigateToCart}
          //  eslint-disable-next-line react-native/no-raw-text
        >
          Continue
        </ButtonWithBackground> */}
        <RNSampleModal
          isVisible={this.state.showOTPModal}
          onRequestClose={() =>
            this.setState({
              showOTPModal: false
            })
          }
          onBackdropPress={() =>
            this.setState({
              showOTPModal: false
            })
          }
          style={homeStyles.modal}
        />
      </KeyboardAwareScrollView>
    );
  }
}

export default HomeScreen;
