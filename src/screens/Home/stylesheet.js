/* eslint-disable react-native/sort-styles */
import { StyleSheet } from "react-native";
import { BUTTON_BORDER_COLOR, TRANSPARENT } from "../../../colors";

export const styles = StyleSheet.create({
  button: {
    borderColor: BUTTON_BORDER_COLOR
    //marginTop: 30
  },
  btnSignUp: {
    alignSelf: "center",
    bottom: 2,
    marginTop: 10,
    paddingHorizontal: 30
  },
  container: {
    flex: 1
  },
  header: {
    marginLeft: 20
  },
  textInputButton: {
    borderColor: BUTTON_BORDER_COLOR,
    borderRadius: 5,
    left: 105,
    bottom: 47,
    height: "39%",
    width: "28%"
  },
  // eslint-disable-next-line react-native/no-color-literals
  textInput: {
    borderColor: "gray",
    borderWidth: 1,
    height: "15%",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 120,
    padding: 5,
    width: "60%"
  },
  modal: {
    backgroundColor: TRANSPARENT,
    borderRadius: 2
  }
});
