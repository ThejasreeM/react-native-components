import React, { Component } from "react";
import { Image, Text, View, Dimensions } from "react-native";
import { Marker } from "react-native-maps";

// const height = h(100);
// const width = w(100);
const { height, width } = Dimensions.get("window");
const totalSize = num =>
  (Math.sqrt(height * height + width * width) * num) / 100;

export default class CustomMarker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      props: {},
      point_count: 0,
      clusterId: null
    };
  }

  shouldComponentUpdate(nextProps) {
    if (
      this.state.geometry === nextProps.geometry &&
      this.state.point_count === nextProps.properties.point_count
    ) {
      return false;
    }
    return true;
  }

  onMarkerPress = () => {
    const markers = superCluster.getLeaves(this.state.clusterId, 5000, 0);
    this.props.onClusterPress(
      {
        longitude: this.props.geometry.coordinates[0],
        latitude: this.props.geometry.coordinates[1]
      },
      markers
    );
  };

  render() {
    this.state.point_count = this.props.properties.point_count;
    if (this.state.point_count === 0) {
      this.state.props = this.props.item.props;
    } else {
      this.state.clusterId = this.props.properties.cluster_id;
      this.state.props = {};
    }

    let textForCluster = "";
    let markerWidth, markerHeight, textSize;
    let point_count = this.state.point_count;

    if (point_count >= 2 && point_count <= 10) {
      textForCluster = point_count.toString();
      markerWidth = (width * 2) / 15;
      markerHeight = (width * 2) / 15;
    }
    if (point_count > 10 && point_count <= 25) {
      textForCluster = "10+";
      markerWidth = width / 7;
      markerHeight = width / 7;
    }
    if (point_count > 25 && point_count <= 50) {
      textForCluster = "25+";
      markerWidth = (width * 2) / 13;
      markerHeight = (width * 2) / 13;
    }
    if (point_count > 50 && point_count <= 100) {
      textForCluster = "50+";
      markerWidth = width / 6;
      markerHeight = width / 6;
    }
    if (point_count > 100) {
      textForCluster = "100+";
      markerWidth = (width * 2) / 11;
      markerHeight = (width * 2) / 11;
    }
    textSize = totalSize(this.props.clusterStyle.clusterTextSize);
    let htmlElement;
    let isCluster;
    if (textForCluster !== "") {
      isCluster = 1;
      if (
        this.props.customClusterMarkerDesign &&
        typeof this.props.customClusterMarkerDesign === "object"
      ) {
        htmlElement = (
          <View
            style={{
              width: markerWidth,
              height: markerHeight,
              justifyContent: "center",
              alignItems: "center",
              zIndex: 3000
            }}
          >
            {this.props.customClusterMarkerDesign}
            <Text
              style={{
                width: markerWidth,
                textAlign: "center",
                position: "absolute",
                fontSize: textSize,
                backgroundColor: "transparent",
                color: this.props.clusterStyle.clusterTextColor,
                fontWeight: "bold"
              }}
              children={textForCluster}
            />
          </View>
        );
      } else {
        htmlElement = (
          <View
            style={{
              zIndex: 3000,
              borderRadius: markerWidth,
              position: "relative",
              backgroundColor: this.props.clusterStyle.clusterColor,
              width: markerWidth,
              height: markerHeight,
              borderWidth: this.props.clusterStyle.clusterBorderWidth,
              borderColor: this.props.clusterStyle.clusterBorderColor,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                width: markerWidth,
                textAlign: "center",
                fontSize: textSize,
                backgroundColor: "transparent",
                color: this.props.clusterStyle.clusterTextColor,
                fontWeight: "bold"
              }}
            >
              {textForCluster}
            </Text>
          </View>
        );
      }
    } else {
      isCluster = 0;
      htmlElement = this.props.item;
    }

    if (isCluster === 1) {
      if (this.props.onClusterPress) {
        return (
          <Marker
            key={isCluster}
            {...this.state.props}
            coordinate={{
              longitude: this.props.geometry.coordinates[0],
              latitude: this.props.geometry.coordinates[1]
            }}
            style={{ zIndex: 3000 }}
            onPress={this.onMarkerPress}
          >
            {htmlElement}
          </Marker>
        );
      } else {
        return (
          <Marker
            key={isCluster}
            style={{ zIndex: 3000 }}
            coordinate={{
              longitude: this.props.geometry.coordinates[0],
              latitude: this.props.geometry.coordinates[1]
            }}
            {...this.state.props}
          >
            {htmlElement}
          </Marker>
        );
        å;
      }
    } else {
      return htmlElement;
    }
  }
}
