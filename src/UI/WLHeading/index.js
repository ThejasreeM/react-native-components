import React from "react";
import { Text } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";

const WLHeading = props => {
  return (
    <Text style={[styles.heading, props.style]} {...props}>
      {props.children}
    </Text>
  );
};

WLHeading.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};
export default WLHeading;
