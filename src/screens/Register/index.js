import React, { Component } from "react";
import { View, Text, StatusBar, Platform, Alert, Button } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { styles as registerStyles } from "./stylesheet";
import WLTextInput from "../../UI/WLTextInput";
import WLButton from "../../UI/WLButton";
import WLText from "../../UI/WLText";
import { Container, StyleProvider } from "native-base";
import { InputValidator, formValidator } from "../../Utilities/Validators";
import PropTypes from "prop-types";
import NBHeader from "../../components/NBHeader";
import getTheme from "../../../native-base-theme/components";
import commonColor from "../../../native-base-theme/variables/commonColor";
// import Modal from "react-native-modal";
import RNSampleModal from "../../components/RNSampleModal";
import alert from "../../components/Alert";

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOTPModal: false,
      signUpForm: {
        firstName: {
          title: "First Name",
          value: "",
          type: "default",
          isSecure: false,
          showPassword: true,
          success: false,
          error: false,
          helpText: "",
          validators: {
            required: true
          }
        },
        lastName: {
          title: "Last Name",
          value: "",
          type: "default",
          isSecure: false,
          showPassword: true,
          success: false,
          error: false,
          helpText: "",
          validators: {
            required: true
          }
        },
        city: {
          title: "City",
          value: "",
          type: "default",
          isSecure: false,
          showPassword: true,
          success: false,
          error: false,
          helpText: "",
          validators: {
            required: true
          }
        },
        // phoneNumber: {
        //   title: "Phone Number",
        //   value: "",
        //   // type: Platform.OS === "android" ? "phone-pad" : "number-pad",
        //   isSecure: false,
        //   showPassword: true,
        //   success: false,
        //   error: false,
        //   helpText: "",
        //   validators: {
        //     required: false
        //   }
        // },
        email: {
          title: "Email",
          value: "",
          type: "email-address",
          isSecure: false,
          showPassword: true,
          success: false,
          error: false,
          helpText: "",
          validators: {
            required: true,
            email: true
          }
        }
        // password: {
        //   title: "Password",
        //   value: "",
        //   type: "default",
        //   isSecure: true,
        //   success: false,
        //   error: false,
        //   helpText: "",
        //   validators: {
        //     required: true,
        //     minlength: 8
        //   }
        // },
        // confirmPassword: {
        //   title: "Confirm Password",
        //   value: "",
        //   type: "default",
        //   isSecure: true,
        //   success: false,
        //   error: false,
        //   helpText: "",
        //   validators: {
        //     required: true,
        //     minlength: 8
        //   }
        //}
      }
    };
  }

  // static navigationOptions = () => {
  //   return {
  //     headerTitle: "Register",
  //     headerTintColor: "royalblue",
  //     headerStyle: {
  //       backgroundColor: "orange"
  //     }
  //   };
  // };
  static navigationOptions = () => {
    return {
      header: null
    };
  };
  navigateToHome = () => {
    // eslint-disable-next-line react/prop-types
    this.props.navigation.navigate("Home");
  };
  // on change text input
  handleInput = (value, field) => {
    const { signUpForm } = { ...this.state };
    signUpForm[field].value = value;
    const validation = InputValidator(
      signUpForm[field].validators,
      value,
      signUpForm[field].title
    );

    signUpForm[field].success = validation.isValid;
    signUpForm[field].error = !signUpForm[field].success;
    signUpForm[field].helpText = validation.helperText;
    this.setState({ signUpForm: signUpForm });
  };

  validateForm() {
    const { signUpForm } = { ...this.state };
    const isValid = formValidator(signUpForm);
    this.setState({ signUpForm: signUpForm });
    return isValid;
  }

  // on press sign up
  onPressSignUp = () => {
    //  const { signUpForm } = { ...this.state };
    if (!this.validateForm()) {
      Alert.alert("", "Please Enter Valid Details", [{ text: "OK" }], {
        cancelable: false
      });
    } else {
      this.props.navigation.navigate("Home");
    }
  };
  onNext = () => {
    this.props.navigation.navigate("Login");
  };
  renderModalContent = () => (
    <View style={registerStyles.content}>
      <Text style={registerStyles.contentTitle}>Hi 👋!</Text>
      <Button
        onPress={() => this.setState({ visibleModal: null })}
        title="Close"
      />
    </View>
  );
  checkAlert = () => {
    let title = "Sample";
    alert(title);
  };
  render() {
    const inputs = Object.keys(this.state.signUpForm).map((field, index) => {
      if (!this.state.signUpForm[field].isSecure) {
        return (
          <WLTextInput
            placeholder={this.state.signUpForm[field].title}
            keyboardType={this.state.signUpForm[field].type}
            onChangeText={e => this.handleInput(e, field)}
            value={this.state.signUpForm[field].value}
            success={this.state.signUpForm[field].success}
            error={this.state.signUpForm[field].error}
            helperText={this.state.signUpForm[field].helpText}
            key={index}
          />
        );
      }
    });

    return (
      <KeyboardAwareScrollView scrollEnabled={true}>
        <StyleProvider style={getTheme(commonColor)}>
          <Container style={registerStyles.container}>
            {/* Header */}
            <NBHeader search={true} title="Register" />
            <StatusBar
              barStyle={
                Platform.OS === "ios" ? "dark-content" : "light-content"
              }
              backgroundColor="gray"
            />

            <View>{inputs}</View>
            {/*Sign Up button*/}

            <WLButton
              onPress={this.onPressSignUp}
              style={registerStyles.btnSignUp}
            >
              <WLText>
                <Text>SIGN UP</Text>
              </WLText>
            </WLButton>
            <View
              style={{ justifyContent: "space-around", flexDirection: "row" }}
            >
              {/* <WLButton onPress={() => this.props.navigation.navigate("Cart")}>
                <WLText>
                  <Text>NEXT</Text>
                </WLText>
              </WLButton> */}
              <View>
                {/* <WLButton
                  style={registerStyles.btnSignUp}
                  onPress={this.checkAlert}
                >
                  <WLText>
                    <Text>CONTINUE</Text>
                  </WLText>
                </WLButton> */}
              </View>
            </View>

            {/* <WLButton
              style={registerStyles.btnSignUp}
              onPress={() => this.props.navigation.navigate("Orders")}
            >
              <WLText>
                <Text>CONTINUE</Text>
              </WLText>
            </WLButton> */}

            {/* <WLButton
              style={registerStyles.btnSignUp}
              onPress={() => this.setState({ showOTPModal: true })}
            >
              <WLText>
                <Text>Modal</Text>
              </WLText>
            </WLButton> */}

            {/*Modal*/}
            <RNSampleModal
              isVisible={this.state.showOTPModal}
              onRequestClose={() =>
                this.setState({
                  showOTPModal: false
                })
              }
              onBackdropPress={() =>
                this.setState({
                  showOTPModal: false
                })
              }
              style={registerStyles.modal}
            />
          </Container>
        </StyleProvider>
      </KeyboardAwareScrollView>
    );
  }
}

export default RegisterScreen;
RegisterScreen.propTypes = {
  navigation: PropTypes.object
};
