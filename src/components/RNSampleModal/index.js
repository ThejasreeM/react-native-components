import React from "react";
import { Text, View } from "react-native";
import WLPopupModal from "../../UI/WLPopupModal";
import styles from "./stylesheet";

class OTPPopupModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {};

  render() {
    return (
      <View>
        <WLPopupModal {...this.props}>
          <View style={styles.modal}>
            <Text>Sample Modal</Text>
          </View>
        </WLPopupModal>
      </View>
    );
  }
}

export default OTPPopupModal;
