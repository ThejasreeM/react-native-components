/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from "react";
import { View } from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import styles from "./stylesheet";
// import MapView from "react-native-map-clustering";
import PropTypes from "prop-types";
import { markersData } from "../../Json/markers";
let latDelta = 0;
let longDelta = 0;
import Icon from "react-native-vector-icons/FontAwesome5";

class Maps extends Component {
  mapStyle = [
    {
      featureType: "landscape",
      stylers: [
        {
          hue: "#FFBB00"
        },
        {
          saturation: 43.400000000000006
        },
        {
          lightness: 37.599999999999994
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "road.highway",
      stylers: [
        {
          hue: "#FFC200"
        },
        {
          saturation: -61.8
        },
        {
          lightness: 45.599999999999994
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "road.arterial",
      stylers: [
        {
          hue: "#FF0300"
        },
        {
          saturation: -100
        },
        {
          lightness: 51.19999999999999
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "road.local",
      stylers: [
        {
          hue: "#FF0300"
        },
        {
          saturation: -100
        },
        {
          lightness: 52
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "water",
      stylers: [
        {
          hue: "#0078FF"
        },
        {
          saturation: -13.200000000000003
        },
        {
          lightness: 2.4000000000000057
        },
        {
          gamma: 1
        }
      ]
    },
    {
      featureType: "poi",
      stylers: [
        {
          hue: "#00FF6A"
        },
        {
          saturation: -1.0989010989011234
        },
        {
          lightness: 11.200000000000017
        },
        {
          gamma: 1
        }
      ]
    }
  ];
  userCurrentLocation = {
    latitude: "",
    longitude: ""
  };
  constructor(props) {
    super(props);
    this.state = {
      mapRegion: {
        latitude: 0.0,
        longitude: 0.0,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      region: props.region
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Maps",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      },
      headerLeft: (
        <Icon
          name={"arrow-left"}
          size={26}
          style={{
            marginLeft: 20
          }}
          onPress={() => {
            navigation.navigate("Home");
          }}
        />
      )
    };
  };
  componentDidMount() {
    setTimeout(() => this.setState({ flex: 1 }), 500);
  }

  componentWillMount = () => {
    //this.getInitialState();
  };

  setMapRef = ref => {
    this.mapView = ref;
  };
  clusterPress = (coordinates, markers) => {
    console.log("hi", coordinates[0]);
    console.log("markers:", markers);
    if (longDelta > 0.000774823129 && latDelta > 0.0011657993039) {
      let coord = [];
      for (let marker of markers) {
        coord.push({
          latitude: marker.geometry.coordinates[1],
          longitude: marker.geometry.coordinates[0]
        });
      }
      this.mapView._root.fitToCoordinates(coord, {
        edgePadding: { top: 250, right: 100, bottom: 250, left: 100 },
        animated: true
      });
    }
  };
  _onRegionChange = region => {
    clearTimeout(this.timerForMap);
    this.timerForMap = setTimeout(() => {
      this.setState({ region });
    }, 500);
  };
  render() {
    //console.log("props::", this.props);
    //console.log(markersData);
    const customMarkers = markersData.map(marker => {
      //console.log(marker);
      return (
        <Marker
          key={marker.id}
          coordinate={{
            latitude: marker.lat,
            longitude: marker.lng
          }}
          title={marker.title}
          cluster={true}
          // image={require("../../assets/images/checked.png")}
        />
      );
    });

    return (
      <View style={styles.container}>
        <MapView
          style={[{ flex: this.state.flex }, this.props.style]}
          ref={this.setMapRef}
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          showsUserLocation={true}
          showsMyLocationButton={true}
          followsUserLocation={true}
          showsCompass={true}
          cacheEnabled={false}
          loadingEnabled={true}
          toolbarEnabled={false}
          zoomEnabled={true}
          //customMapStyle={this.mapStyle}
          initialRegion={this.props.initialRegion}
          region={this.props.region}
          // radius={15}
          // clustering={true}
          // clusterTextColor="#000"
          // clusterBorderWidth={3}
          // clusterTextSize={15}
          // clusterColor={"#F5F5F5"}
          // clusterBorderColor={"#FF5252"}
          //onPress={this.props.onPress}
          //onClusterPress={this.clusterPress}
          // onClusterPress={(coords, markers) => {
          //   console.log(coords);
          //   console.log(markers);
          // }}
          //onRegionChangeComplete={this._onRegionChange}
          //onRegionChangeComplete={this.props.onRegionChangeComplete}
        >
          <Marker
            coordinate={this.props.currentLocation}
            // image={require("../../assets/images/checked.png")}
            cluster={true}
          />
          {/* {customMarkers} */}
        </MapView>
      </View>
    );
  }
}

// prop types
Maps.propTypes = {
  mapMarkers: PropTypes.array,
  onPressMarker: PropTypes.func,
  region: PropTypes.object,
  currentLocation: PropTypes.object,
  initialRegion: PropTypes.object,
  onRegionChange: PropTypes.func,
  onPress: PropTypes.func,
  style: PropTypes.object,
  children: PropTypes.array,
  onRegionChangeComplete: PropTypes.func
};
export default Maps;
