import React from "react";
import { Button } from "native-base";
import styles from "./stylesheet";
import PropTypes from "prop-types";

const WLNBButton = props => {
  return (
    <Button style={[styles.button, props.style]} {...props}>
      {props.children}
    </Button>
  );
};

WLNBButton.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};

export default WLNBButton;
