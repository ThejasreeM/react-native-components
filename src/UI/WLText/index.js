import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";
import styles from "./stylesheet";

const WLText = props => {
  return (
    <Text style={[styles.text, props.style]} {...props}>
      {props.children}
    </Text>
  );
};

WLText.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};

export default WLText;
