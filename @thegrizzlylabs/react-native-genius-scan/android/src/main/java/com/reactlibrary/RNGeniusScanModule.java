
package com.reactlibrary;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import com.thegrizzlylabs.geniusscan.sdk.pdf.PDFGeneratorError;
import com.geniusscan.GeniusScanSdkUI;
import com.geniusscan.PromiseResult;
import com.geniusscan.enhance.PdfGenerationTask;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public class RNGeniusScanModule extends ReactContextBaseJavaModule {
  private static final String E_PDF_ERROR = "E_PDF_GENERATION_ERROR";
  private final ReactApplicationContext reactContext;
  private Promise mScanPromise;

  public RNGeniusScanModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    // Add the listener for `onActivityResult`
    reactContext.addActivityEventListener(mActivityEventListener);
  }

  @Override
  public String getName() {
    return "RNGeniusScan";
  }

  // We don't work directly with Promise here as it is a React Native Bridge module
  private void resolvePromiseWithPromiseResult(Promise promise, PromiseResult promiseResult) {
    if (promiseResult.isError) {
      promise.reject(promiseResult.code, promiseResult.message);
    } else if (promiseResult.resultMap != null) {
      WritableMap map = new WritableNativeMap();
      for (Map.Entry<String, String> entry : promiseResult.resultMap.entrySet()) {
        map.putString(entry.getKey(), entry.getValue());
      }
      promise.resolve(map);
    } else {
      promise.resolve(promiseResult.result);
    }
  }

  private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
      PromiseResult promiseResult = GeniusScanSdkUI.resolvePromiseWithActivityResult(activity, requestCode, resultCode, intent);
      if (promiseResult != null) {
        resolvePromiseWithPromiseResult(mScanPromise, promiseResult);
        mScanPromise = null;
      }
    }
  };

  @SuppressWarnings("unused")
  @ReactMethod
  public void scanCamera(ReadableMap scanOptions, Promise promise) {
    mScanPromise = promise;
    GeniusScanSdkUI.scanCamera(getCurrentActivity(), scanOptions.toHashMap());
  }

  @SuppressWarnings("unused")
  @ReactMethod
  public void scanImage(String imageUri, ReadableMap scanOptions, Promise promise) {
    mScanPromise = promise;
    GeniusScanSdkUI.scanImage(getCurrentActivity(), imageUri, scanOptions.toHashMap());
  }

  @SuppressWarnings("unused")
  @ReactMethod
  public void setLicenceKey(
      String licenceKey,
      Promise promise) {
    resolvePromiseWithPromiseResult(
      promise,
      GeniusScanSdkUI.setLicenceKey(this.reactContext, licenceKey)
    );
  }

  @SuppressWarnings("unused")
  @ReactMethod
  public void generatePDF(String title, ReadableArray imageUriStrings, ReadableMap pdfOptions, final Promise promise) {
    File outputFile = new File(this.reactContext.getExternalCacheDir(), title + ".pdf");
    final String absoluteFilePath = outputFile.getAbsolutePath();

    ArrayList<String> imageUris = new ArrayList<String>();
    for (int i=0; i<imageUriStrings.size(); i++) {
        Uri imageFileUri = Uri.parse(imageUriStrings.getString(i));
        imageUris.add( imageFileUri.getPath());
    }

    new PdfGenerationTask(title, imageUris, absoluteFilePath, pdfOptions.toHashMap(), new PdfGenerationTask.OnPdfGeneratedListener() {
        @Override
        public void onPdfGenerated(PDFGeneratorError result) {
            if (result != PDFGeneratorError.PDFGENERATORERRORCODESUCCESS) {
                promise.reject(E_PDF_ERROR, "Error generating PDF: " + result);
            } else {
                promise.resolve(absoluteFilePath);
            }
        }
    }).execute();
  }
}