/* eslint-disable no-unused-vars */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer
} from "react-navigation";
import LoginScreen from "./src/screens/Login";
import RegisterScreen from "./src/screens/Register";
import HomeScreen from "./src/screens/Home";
import CartScreen from "./src/screens/Cart";
import OrdersListScreen from "./src/screens/OrdersList";
//import tabsNavigator from "./src/components/TabNavigation";
import CardScreen from "./src/screens/Card";
import CardListScreen from "./src/screens/CardList";
import ProfileScreen from "./src/screens/Profile";
import SliderScreen from "./src/screens/Slider";
import ScannerScreen from "./src/screens/Scanner";
import DialScreen from "./src/screens/Dial";

const AuthStack = createStackNavigator({
  Register: {
    screen: RegisterScreen
  },
  Login: {
    screen: LoginScreen
  }
});

const AppStack = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  Cart: {
    screen: CartScreen
  },
  Orders: {
    screen: OrdersListScreen
  },
  Card: {
    screen: CardScreen
  },
  CardList: {
    screen: CardListScreen
  },
  Profile: {
    screen: ProfileScreen
  },
  Slider: {
    screen: SliderScreen
  },
  Scanner: {
    screen: ScannerScreen
  },
  Dial: {
    screen: DialScreen
  }
});

const AppNavigator = createSwitchNavigator(
  {
    Auth: AuthStack,
    App: AppStack
  },
  {
    initialRouteName: "Auth"
  }
);

const AppContainer = createAppContainer(AppNavigator);

class App extends Component {
  render() {
    return <AppContainer />;
  }
}
export default App;
