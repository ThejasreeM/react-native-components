import React, { Component } from "react";
import { TextInput, TouchableOpacity, View, Text } from "react-native";
import styles from "./stylesheet";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/FontAwesome";
import WLText from "../WLText";

export default class WLPasswordInput extends Component {
  static propTypes = {
    style: PropTypes.object,
    helperText: PropTypes.string
  };

  state = {
    showPassword: true
  };

  onPressEye = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  render() {
    const { helperText } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.textInputView}>
          <TextInput
            placeholderTextColor="grey"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            secureTextEntry={this.state.showPassword}
            style={[styles.textInput, this.props.style]}
            {...this.props}
          />
          <TouchableOpacity onPress={this.onPressEye} style={styles.eye}>
            <View>
              <Icon
                name={this.state.showPassword ? "eye" : "eye-slash"}
                size={20}
                color="gray"
              />
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <WLText>
            <Text style={styles.textStyle}>{helperText}</Text>
          </WLText>
        </View>
      </View>
    );
  }
}
