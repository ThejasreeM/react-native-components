import { Dimensions, StyleSheet } from "react-native";
import { MODAL_BACKGROUND_COLOR } from "../../../colors";

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  modal: {
    alignItems: "center",
    backgroundColor: MODAL_BACKGROUND_COLOR,
    height: height / 3,
    justifyContent: "center",
    width: width / 1.12
  }
});
export default styles;
