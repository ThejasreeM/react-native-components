import React from "react";
import { Text } from "native-base";
import PropTypes from "prop-types";
import styles from "./stylesheet";

const WLNBText = props => {
  return (
    <Text style={[styles.text, props.style]} {...props}>
      {props.children}
    </Text>
  );
};

WLNBText.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object
};

export default WLNBText;
