/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
/* eslint-disable no-undef */
import React from "react";
import { Platform } from "react-native";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import Icon from "react-native-vector-icons/Ionicons";
import CartScreen from "../../screens/Cart";
import LibraryScreen from "../../screens/Library";
import AlbumScreen from "../../screens/Album";
// import PropTypes from "prop-types";

// export default tabsNavigator = createMaterialBottomTabNavigator(
//   {
//     Album: { screen: AlbumScreen },
//     Library: { screen: LibraryScreen },
//     Cart: { screen: CartScreen }
//   },
//   {
//     initialRouteName: "Album",
//     activeColor: "#f0edf6",
//     inactiveColor: "#3e2465",
//     barStyle: { backgroundColor: "grey" }
//   }
// );

export default tabsNavigator = createMaterialBottomTabNavigator(
  {
    Album: {
      screen: AlbumScreen,
      navigationOptions: {
        title: "Album",
        tabBarLabel: "Album",
        tabBarColor: "#842655",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            size={26}
            name={
              Platform.OS === "ios"
                ? focused
                  ? "ios-home"
                  : "ios-home-outline"
                : "md-home"
            }
            style={{ color: tintColor }}
          />
        )
      }
    },
    Library: {
      screen: LibraryScreen,
      navigationOptions: {
        tabBarLabel: "Library",
        tabBarColor: "#1e1e1d",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            size={26}
            name={
              Platform.OS === "ios"
                ? focused
                  ? "ios-contact"
                  : "ios-contact-outline"
                : "md-contact"
            }
            style={{ color: tintColor }}
          />
        )
      }
    },
    Cart: {
      screen: CartScreen,
      navigationOptions: {
        tabBarLabel: "Cart",
        tabBarColor: "#ff3838",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            size={26}
            name={
              Platform.OS === "ios"
                ? focused
                  ? "ios-settings"
                  : "ios-settings-outline"
                : "md-settings"
            }
            style={{ color: tintColor }}
          />
        )
      }
    }
  },
  {
    //the active tab appears wider and the inactive tabs won't have a label.
    // By default, this is true when you have more than 3 tabs.
    shifting: false,
    // activeTintColor: "white",
    //inactiveTintColor: "#ddd",
    activeColor: "white",
    inactiveColor: "black",
    barStyle: {
      height: 55,
      backgroundColor: "#9d8ace"
    }
  }
);

// tabsNavigator.propTypes = {
//   tintColor: PropTypes.bool,
//   focused: PropTypes.bool
// };
