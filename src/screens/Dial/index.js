// /* eslint-disable react-native/no-inline-styles */
// /* eslint-disable react-native/no-unused-styles */
// /* eslint-disable react-native/no-color-literals */
// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, { Component } from "react";
// import { Platform, StyleSheet, Text, View, Button, Image } from "react-native";
// import RNGeniusScan from "@thegrizzlylabs/react-native-genius-scan";
// // const instructions = Platform.select({
// //   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
// //   android:
// //     'Double tap R on your keyboard to reload,\n' +
// //     'Shake or press menu button for dev menu',
// // });
// //import RNGeniusScan from "./@thegrizzlylabs/react-native-genius-scan";

// type Props = {};
// class CamScannerScreen extends Component<Props> {
//   state = {
//     image: null,
//     originalImage: null
//   };
//   render() {
//     return (
//       <View style={styles.container}>
//         <View style={{ height: 150 }}>
//           {this.state.image && (
//             <Image
//               style={{
//                 width: 150,
//                 height: 150
//               }}
//               resizeMode="contain"
//               source={{ uri: this.state.image }}
//             />
//           )}
//         </View>
//         <View style={styles.button}>
//           <Button
//             onPress={() => {
//               RNGeniusScan.scanCamera()
//                 .then(result =>
//                   this.setState({
//                     image: result["enhancedImageUri"],
//                     originalImage: result["originalImageUri"]
//                   })
//                 )
//                 .catch(e => alert(e));
//             }}
//             title="Scan with camera"
//           />
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   button: {
//     margin: 5
//   },
//   container: {
//     alignItems: "center",
//     backgroundColor: "#F5FCFF",
//     flex: 1,
//     justifyContent: "center"
//   },
//   controls: {
//     alignItems: "center",
//     flexDirection: "column"
//   }
// });
// export default CamScannerScreen;

import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import call from "react-native-phone-call";
import DocumentPicker from "react-native-document-picker";

class DialScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: "+91-9111111111",
      document: null
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Call",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      }
      //   headerLeft: (
      //     <Icon
      //       name={"arrow-left"}
      //       size={24}
      //       style={homeStyles.header}
      //       onPress={() => {
      //         navigation.navigate("Register");
      //       }}
      //     />
      //   )
    };
  };
  onPress = () => {
    const args = {
      number: this.state.number, // String value with the number to call
      prompt: true // Optional boolean property. Determines if the user should be prompt prior to the call
    };

    call(args).catch(console.error);
  };
  onDocumentPicker = async () => {
    // Pick a single file
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images]
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
      alert(res.uri);
      this.setState({ document: res.uri });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity onPress={this.onPress}>
          <View style={{ alignSelf: "center", marginTop: 30 }}>
            <Text>{this.state.number}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.onDocumentPicker}>
          <View style={{ alignSelf: "center", marginTop: 30 }}>
            <Text>Document</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default DialScreen;
