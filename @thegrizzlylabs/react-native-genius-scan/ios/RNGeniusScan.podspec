
Pod::Spec.new do |s|
  s.name         = "RNGeniusScan"
  s.version      = "1.0.0"
  s.summary      = "GeniusScan plugin for React Native"
  s.description  = <<-DESC
                  RNGeniusScan
                   DESC
  s.homepage     = ""
  s.license      = "MIT"
  s.author           = { 'The Grizzly Labs' => 'contact@thegrizzlylabs.com' }
  s.homepage         = "http://thegrizzlylabs.com"
  s.source_files  = "RNGeniusScan/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

