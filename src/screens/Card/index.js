/* eslint-disable react-native/no-raw-text */
import React, { Component } from "react";
import PropTypes from "prop-types";
import WLMainCard from "../../UI/WLMainCard";
import { Container } from "native-base";
// import CustomFooter from "../../components/FooterTab";

class CardScreen extends Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    title: "Card"
  };
  onPressIcon = () => {
    this.props.navigation.navigate("Card");
  };
  render() {
    return (
      <Container style={{ flex: 1 }}>
        <WLMainCard navigation={this.props.navigation} />
        {/* <CustomFooter /> */}
      </Container>
    );
  }
}

export default CardScreen;

CardScreen.propTypes = {
  navigation: PropTypes.object
};
