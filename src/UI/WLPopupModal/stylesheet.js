import { Dimensions, StyleSheet } from "react-native";

const { deviceWidth, deviceHeight } = Dimensions.get("window");
const styles = StyleSheet.create({
  modal: {
    //backgroundColor: 'blue',
    height: deviceHeight / 3,
    width: deviceWidth / 3
  }
});
export default styles;
