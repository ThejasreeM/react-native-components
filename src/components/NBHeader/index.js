/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-raw-text */
import React from "react";
import { View, TouchableOpacity } from "react-native";
import { Header, Button, Title, Icon, Left, Right, Body } from "native-base";
import PropTypes from "prop-types";

const NBHeader = ({ isBack, navigation, search, title }) => {
  const onPressBack = () => {
    navigation.goBack();
  };

  return (
    <View>
      <Header>
        <Left>
          {isBack ? (
            <TouchableOpacity onPress={onPressBack}>
              <Button transparent>
                <Icon
                  name="ios-arrow-back"
                  style={{
                    alignSelf: "flex-start",
                    justifyContent: "flex-start",
                    alignItems: "flex-start"
                  }}
                />
              </Button>
            </TouchableOpacity>
          ) : (
            <Button transparent>
              <Icon
                name="ios-menu"
                style={{
                  alignSelf: "flex-start",
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  alignContent: "flex-start"
                }}
              />
            </Button>
          )}
        </Left>
        <Body>
          <Title>{title}</Title>
        </Body>
        <Right>
          {search ? (
            <View
              style={{
                justifyContent: "flex-end",
                alignContent: "flex-end",
                alignItems: "flex-end"
              }}
            >
              <Button transparent>
                <Icon name="ios-search" />
              </Button>
            </View>
          ) : (
            <View
              style={{
                justifyContent: "flex-end",
                alignContent: "flex-end",
                alignItems: "flex-end"
              }}
            >
              <Button transparent>
                <Icon name="ios-alert" />
              </Button>
            </View>
          )}
        </Right>
      </Header>
    </View>
  );
};
export default NBHeader;

NBHeader.propTypes = {
  isBack: PropTypes.bool,
  navigation: PropTypes.object,
  search: PropTypes.bool,
  title: PropTypes.string,
  onPressBack: PropTypes.func
};
