/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from "react";
import { View, StyleSheet, Platform, Alert } from "react-native";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
//import Maps from "../../components/Maps";
import MapWithClustering from "../../mapUtils/MapWithClustering";
// import MapsCluster from "../../components/MapCluster";
import Permissions from "react-native-permissions";
import { checkPermissionStatus } from "../../components/Permissions";
import { Marker } from "react-native-maps";
import { MarkersData } from "../../Json/markers";
const region = {
  latitude: 52.5,
  longitude: 19.2,
  latitudeDelta: 8.5,
  longitudeDelta: 8.5
};
class OrdersListScreen extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    userCurrentLocation = {
      latitude: "",
      longitude: "",
      openCameraOnLocation: false
    };
    this.state = {
      mapRegion: {
        latitude: 0.0,
        longitude: 0.0,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      currentLocation: {
        latitude: 0.0,
        longitude: 0.0
      },
      regionName: "",
      marginBottom: 1
    };
  }

  async componentWillMount() {
    let canOpenSettings = Permissions.canOpenSettings();
    this.setState({ canOpenSettings });
    //getting location alert  for permissions
    await this.getInitialState();
    await this.showLocationDialog(true);
  }
  componentDidMount() {
    setTimeout(() => this.setState({ flex: 1 }), 500);
  }
  getInitialState() {
    this.getLocation().then(data => {
      //console.log(data);
      const mapRegion = { ...this.state.mapRegion };
      (mapRegion.latitude = data.latitude),
        (mapRegion.longitude = data.longitude);
      const currentLocation = { ...this.state.currentLocation };
      (currentLocation.latitude = data.latitude),
        (currentLocation.longitude = data.longitude),
        this.setState({
          mapRegion: mapRegion,
          currentLocation: currentLocation
        });
    });
  }

  getLocation = () => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        data => {
          // console.log("getCurrentPosition:", data);
          const mapRegion = { ...this.state.mapRegion };
          mapRegion.latitude = data.coords.latitude;
          mapRegion.longitude = data.coords.longitude;
          this.setState({ mapRegion: mapRegion });
          resolve(data.coords);
        },
        err => reject(err)
      );
    });
  };
  _openSettingsForLocation = () => {
    Permissions.openSettings().then(() => alert("back to app!!"));
  };

  async showLocationDialog() {
    try {
      const locationPermission = await checkPermissionStatus("location");
      if (locationPermission.status) {
        const data = await RNAndroidLocationEnabler.promptForEnableLocationIfNeeded(
          { interval: 10000, fastInterval: 5000 }
        );
        // data can be :
        //  - "already-enabled" if the location services has been already enabled
        //  - "enabled" if user has clicked on OK button in the popup
        if (data === "enabled") {
          return true;
        } else {
          return false;
        }
      } else {
        if (Platform.OS === "ios") {
          // go to settings to on location
          const buttons = [{ text: "Cancel", style: "cancel" }];
          if (this.state.canOpenSettings)
            buttons.push({
              text: "Open Settings",
              onPress: this._openSettingsForLocation
            });
          Alert.alert(
            "Oops!",
            "There was a problem getting your permission. Please enable Location from settings.",
            buttons
          );
          return "";
        }
        return false;
      }
    } catch (err) {
      return false;
    }
  }
  setMapRef = ref => {
    this.mapView = ref;
  };
  resetUserPosition = async () => {
    if (
      userCurrentLocation.latitude != "" &&
      userCurrentLocation.longitude != ""
    ) {
      this.mapView._root.animateToRegion({
        latitude: userCurrentLocation.latitude,
        longitude: userCurrentLocation.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      });
    } else {
      const isLocationEnabled = await this.showLocationDialog(false);
      if (isLocationEnabled) {
        await this.resetUserPosition();
      }
    }
  };
  clusterPress = (coordinates, markers) => {
    // console.log("hi", coordinates);
    //console.log("markers:", markers);
    if (longDelta > 0.000774823129 && latDelta > 0.0011657993039) {
      let coord = [];
      for (let marker of markers) {
        coord.push({
          latitude: marker.geometry.coordinates[1],
          longitude: marker.geometry.coordinates[0]
        });
      }
      this.mapView._root.fitToCoordinates(coord, {
        edgePadding: { top: 250, right: 100, bottom: 250, left: 100 },
        animated: true
      });
    }
  };
  _onMapReady = () => this.setState({ marginBottom: 0 });
  resetMapInitially = 0;
  onUserLocationChange = async event => {
    const { coordinate } = event.nativeEvent;
    //console.log(coordinate);
    userCurrentLocation.latitude = coordinate.latitude;
    userCurrentLocation.longitude = coordinate.longitude;
    if (this.resetMapInitially == 0) {
      this.resetMapInitially += 1;
      this.resetUserPosition();
    }
  };
  setLatLngDeltas = reg => {
    latDelta = reg.latitudeDelta;
    longDelta = reg.longitudeDelta;
  };
  render() {
    // console.log(markersData);
    const customMarkers = MarkersData.map(marker => {
      //console.log(marker);
      return (
        <Marker
          key={marker.id}
          coordinate={{
            latitude: marker.lat,
            longitude: marker.lng
          }}
          title={marker.title}
          cluster={true}
          // image={require("../../assets/images/checked.png")}
        />
      );
    });
    return (
      <View style={styles.container}>
        {/* <Maps
          initialRegion={this.state.mapRegion}
          region={this.state.mapRegion ? this.state.mapRegion : region}
          style={{
            marginBottom: this.state.marginBottom
          }}
          //onPress={e => this.onMapPress(e)}
          currentLocation={this.state.currentLocation}
          onMapReady={this._onMapReady}
          onRegionChangeComplete={mapRegion => this.setState({ mapRegion })}
        /> */}
        {/* <MapsCluster
          initialRegion={this.state.mapRegion}
          region={this.state.mapRegion ? this.state.mapRegion : region}
          style={{
            marginBottom: this.state.marginBottom
          }}
          currentLocation={this.state.currentLocation}
          onMapReady={this._onMapReady}
          onRegionChangeComplete={mapRegion => this.setState({ mapRegion })}
        /> */}
        <MapWithClustering
          ref={this.setMapRef}
          initialRegion={this.state.mapRegion}
          //onPress={this.onMapPress}
          onRegionChangeComplete={this.setLatLngDeltas}
          radius={15}
          clusterTextColor="#006E81"
          clusterBorderWidth={5}
          clusterTextSize={2.3}
          clusterColor={"#F5F5F5"}
          clusterBorderColor={"#FF5252"}
          //customClusterMarkerDesign={this.clusters()}
          onUserLocationChange={this.onUserLocationChange}
          onClusterPress={this.clusterPress}
        >
          <Marker
            coordinate={this.state.currentLocation}
            // image={require("../../assets/images/checked.png")}
            cluster={true}
          />
          {customMarkers}
        </MapWithClustering>
      </View>
    );
  }
}

export default OrdersListScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
