/* eslint-disable no-undef */
import "react-native";
import React from "react";
import { expect } from "chai";
import sinon from "sinon";
import Button from "../screens/Home";

describe("<Home />", () => {
  it("simulates click events", () => {
    const checkAlert = sinon.spy();
    const wrapper = mount(<Button checkAlert={checkAlert} />);
    wrapper.find("button").simulate("click");
    expect(checkAlert).to.have.property("callCount", 1);
  });
});
