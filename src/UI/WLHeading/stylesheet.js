import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  heading: {
    fontFamily: "Roboto",
    fontSize: 30,
    fontWeight: "bold",
    margin: 10,
    position: "relative",
    textAlign: "center"
  }
});

export default styles;
