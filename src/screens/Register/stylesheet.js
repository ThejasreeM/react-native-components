/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native";
import { TRANSPARENT } from "../../../colors";
export const styles = StyleSheet.create({
  btnSignUp: {
    alignSelf: "center",
    bottom: 2,
    marginTop: 10,
    paddingHorizontal: 25
  },
  container: {
    flex: 1
  },
  content: {
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "rgba(0, 0, 0, 0.1)",
    borderRadius: 4,
    justifyContent: "center",
    padding: 22
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12
  },
  modal: {
    backgroundColor: TRANSPARENT,
    borderRadius: 2
  }
  // btnView: {
  //   alignSelf: "center",
  //   backgroundColor: "#A52A2A",
  //   borderColor: "#A52A2A",
  //   borderRadius: 8,
  //   borderWidth: 2,
  //   height: height / 15.2,
  //   marginTop: 20,
  //   padding: 5,
  //   width: width / 3.7
  // },
  // button: {
  //   borderColor: BUTTON_BORDER_COLOR,
  //   marginTop: 30
  // },
  // container: {
  //   flex: 1
  // },
  // mainView: {
  //   flexDirection: "column"
  // },
  // plainText: {
  //   fontSize: 16,
  //   fontWeight: "bold",
  //   marginLeft: 20,
  //   marginTop: 20
  // },
  // text: {
  //   alignSelf: "center",
  //   fontSize: 20,
  //   fontWeight: "bold"
  // },
  // textInput: {
  //   borderBottomColor: "gray",
  //   borderBottomWidth: 1,
  //   marginLeft: 20,
  //   marginRight: 20
  // }
});
