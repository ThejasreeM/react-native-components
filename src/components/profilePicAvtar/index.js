import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { styles as avtarStyles } from "./stylesheet";
import Icon from "react-native-vector-icons/FontAwesome5";
import PropTypes from "prop-types";
import ImagePicker from "react-native-image-picker";
import AsyncStorage from "@react-native-community/async-storage";

class ProfileAvtar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      picSource: null
    };
  }
  openCamera = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        //console.log('User cancelled image picker');
      } else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        //file path has come on source
        AsyncStorage.setItem("profileImage", source.uri);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          picSource: source.uri
        });
      }
    });
  };
  // componentDidMount = () => {
  //   AsyncStorage.getItem("profileImage").then(val => {
  //     console.log(val);
  //   });
  // };
  render() {
    return (
      <View style={avtarStyles.imageView}>
        <Image
          source={{ uri: this.state.picSource }}
          style={avtarStyles.imageView}
        />
        <TouchableOpacity onPress={this.openCamera}>
          <View style={avtarStyles.camView}>
            <Icon name="camera" size={20} style={avtarStyles.icon} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ProfileAvtar;
ProfileAvtar.propTypes = {
  openCamera: PropTypes.func,
  picSource: PropTypes.object
};
