
# Genius Scan SDK for React Native

## Description

This React Native component allows you to access the [Genius Scan SDK](https://www.thegrizzlylabs.com/document-scanner-sdk) core features from a React Native application:

  - Automatic document detection
  - Document perspective correction
  - Image enhancement with 4 different modes (Grayscale, Color, Black & white, Photo)

## Licence

This component is based on the Genius Scan SDK for which you need to [setup a licence](#api).
You can already try the "demo" version for free by not setting a licence key, the only limitation being that the app will exit after 60 seconds.

To buy a licence or for any question regarding the SDK, please contact us at sdk@thegrizzlylabs.com.

## Demo application

As an example, you can check our [demo application](https://github.com/thegrizzlylabs/geniusscan-sdk-demo/tree/master/react-native-genius-scan-demo)

## Getting started

From your React Native root folder.

```
$ npm install @thegrizzlylabs/react-native-genius-scan --save
$ react-native link @thegrizzlylabs/react-native-genius-scan
```

### Additional steps on Android

- To your app's `/android/build.gradle` file, add the @thegrizzlylabs/react-native-genius-scan library dependency

```
allprojects {
    repositories {
        mavenLocal()
        jcenter()
        maven {
            // All of React Native (JS, Obj-C sources, Android binaries) is installed from npm
            url "$rootDir/../node_modules/react-native/android"
        }
        flatDir {
            dirs "$rootDir/../node_modules/@thegrizzlylabs/react-native-genius-scan/android/libs"
        }
    }
}
```

- To your app `/android/app/build.gradle`, change minSdkVersion to `19`.


- Add the required permissions in AndroidManifest.xml:

```
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
```

### Additional steps for iOS

Open your Xcode project file (ios/*****.xcodeproj), and:
- Add GSSDK.framework, located in `node_modules/@thegrizzlylabs/react-native-genius-scan/ios`, to your project "Embedded Libraries"

- Add the path `${PROJECT_DIR}/../node_modules/@thegrizzlylabs/react-native-genius-scan/ios` to your project "Framework Search path" (in build )

- Add the required permission to your `Info.plist`
```
NSCameraUsageDescription - “Privacy - Camera Usage Description”.
```

### Manual linking

Instead of using `react-native link`, you can proceed manually. But you will still need to follow the additional steps described above.

#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `@thegrizzlylabs/react-native-genius-scan` and add `RNGeniusScan.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNGeniusScan.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project <kbd>Cmd</kbd>+<kbd>R</kbd>

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNGeniusScanPackage;` to the imports at the top of the file
  - Add `new RNGeniusScanPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':@thegrizzlylabs_react-native-genius-scan'
  	project(':@thegrizzlylabs_react-native-genius-scan').projectDir = new File(rootProject.projectDir, '../node_modules/@thegrizzlylabs/react-native-genius-scan/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':@thegrizzlylabs_react-native-genius-scan')
  	```

## API
### Set the licence key
```javascript
RNGeniusScan.setLicenceKey('REPLACE_WITH_YOUR_LICENCE_KEY')
```

Initialize the SDK with a valid licence key.

It returns a promise that is resolved if the licence key is valid and rejected if it is not.

Note that, for testing purpose, you can also use the plugin without a licence key, but it will only work for 60 seconds.

### Scan an existing image
```javascript
RNGeniusScan.scanPicture(imageUri, scanOptions)
```

Pass the given image through document detection, perspective correction and image enhancement.

It returns a promise that will resolve to the `originalImageUri` and `enhancedImageUri`, or reject with an error message.

| Param | Type | Description |
| --- | --- | --- |
| imageFileUri | String | file URI to the original JPEG file to be transformed with the plugin (Note: it won't be overriden by the plugin) |
| scanOptions | Object | [Scan Options](#scan-options) |

### Scan a picture from the camera
```javascript
RNGeniusScan.scanCamera(scanOptions)
```

Same as `RNGeniusScan.scanPicture` but the initial picture is taken with the camera.

| Param | Type | Description |
| --- | --- | --- |
| scanOptions | Object | [Scan Options](#scan-options) |

### Generate a pdf from images
```javascript
RNGeniusScan.generatePDF(imageFileUris, pdfOptions)
```

Generates a pdf from the list of image URIs given as first param.

It returns a promise that will resolve to the resulting pdf URI, or reject with an error message.

| Param | Type | Description |
| --- | --- | --- |
| imageFileUris | Array<String> | Array of URIs to JPEG files that will be converted, in the given order, into a pdf
| pdfOptions | Object | Options for pdf generation |
| pdfOptions.password | String | Password to protect the pdf |

### Scan options

`scanOptions` can be used to customize the scanning interface:


| Param | Type | Description |
| --- | --- | --- |
| scanOptions.defaultEnhancement | String | Force a specific image enhancement by default. Accepted values: `RNGeniusScan.ENHANCEMENT_NONE`, `RNGeniusScan.ENHANCEMENT_BW`, `RNGeniusScan.ENHANCEMENT_COLOR`, `RNGeniusScan.ENHANCEMENT_PHOTO` |


## Examples

### Scanning a picture from the camera

```javascript
import RNGeniusScan from '@thegrizzlylabs/react-native-genius-scan';

RNGeniusScan.setLicenceKey('REPLACE_WITH_YOUR_LICENCE_KEY')
.then(() => {
	return RNGeniusScan.scanCamera()
})
.then((enhancedImageUri) => {
	// Do something with the enhanced image
})
.catch((error) => {
	// Handle error
})
```

### Scanning an existing picture

```javascript
import RNGeniusScan from '@thegrizzlylabs/react-native-genius-scan';

const imageUri = 'file://xxxxx' // imageUri from an existing file

RNGeniusScan.setLicenceKey('REPLACE_WITH_YOUR_LICENCE_KEY')
.then(() => {
	return RNGeniusScan.scanPicture(imageUri)
})
.then((enhancedImageUri) => {
	// Do something with the enhanced image
})
.catch((error) => {
	// Handle error
})
```

# FAQ

## What if I get a validation error from App Store Connect?

You must remove the x86_64 and i386 slices before submitting your application to the App Store. They are only used for the iOS smiulator and iTunes rejects any binary that contains non-ARM slices.

They can stripped out with a script like [this one](https://stackoverflow.com/a/42642209/1644052).

## What should I do if my license is invalid?

Make sure you have an ongoing contract with us. Contact us at sdk@thegrizzlylabs.com for any information.

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.5.2] - 2019-06-10
### Changed
- [Android] Fixed setLicenceKey method which was not returning properly

## [2.5.1] - 2019-05-07
### Changed
- Fixed typos
- [Android] Fixed layout issue

## [2.5.0] - 2019-04-30
### Changed
- Page format for PDF generation is FIT rather than A4
- PDF title is used for filename, in addition to PDF metadata
- [Android] Image type was incorrectly detected on original image, instead of warped image

### Added
- Document detection status
- Scan functions now return original image Uri, in addition to enhanced image

## [2.4.2] - 2019-02-27
### Added
- `ENHANCEMENT_NONE` default enhancement option
- Cancel button on iOS camera screen
- Auto trigger in Android

## [2.4.1] - 2018-12-05
### Changed
- Fix wrong method name to set license key

## [2.4.0] - 2018-12-05
### Added
- PDF generation with `generatePDF`
- Selecting an enhancement type by default with `defaultEnhancement` option

### Changed
- Fix crash on ipad when clicking "Edit" button
- Fix error with too long activity result code