import { StyleSheet } from "react-native";
import { COLOR_DARK_GREY } from "../../../colors";

export const styles = StyleSheet.create({
  colorGrey: {
    color: COLOR_DARK_GREY
  },

  container: {
    flex: 1,
    paddingBottom: 20
  },

  flexStyle: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },

  overflowStyle: {
    overflow: "hidden"
  },

  sliderStyle: {
    borderRadius: 50,
    height: 20,
    width: 300
  },

  textCon: {
    flexDirection: "row",
    justifyContent: "center",
    width: 300
  }
});
