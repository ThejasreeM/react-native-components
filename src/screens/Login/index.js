import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import { styles as loginStyles } from "./stylesheet";
import PropTypes from "prop-types";
import WLCard from "../../UI/WLCard";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }
  static navigationOptions = {
    title: "Login"
  };

  render() {
    return (
      <View style={loginStyles.container}>
        <Text style={loginStyles.text}>Orders</Text>
        <ScrollView>
          <WLCard checked={false} />
          <WLCard checked={false} />
          <WLCard checked={true} />
          <WLCard checked={false} />
          <WLCard checked={false} />
          <WLCard checked={true} />
          <WLCard checked={true} />
          <WLCard checked={false} />
          <WLCard checked={true} />
          <WLCard checked={true} />
        </ScrollView>
      </View>
    );
  }
}

export default LoginScreen;

LoginScreen.propTypes = {
  navigation: PropTypes.object
};
