import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import { styles as listStyles } from "./stylesheet";
import PropTypes from "prop-types";
import WLCard from "../../UI/WLCard";

class CardListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }
  static navigationOptions = {
    title: "CardList"
  };

  render() {
    return (
      <View style={listStyles.container}>
        <Text style={listStyles.text}>Orders</Text>
        <ScrollView>
          <WLCard checked={false} />
          <WLCard checked={false} />
          <WLCard checked={true} />
          <WLCard checked={false} />
          <WLCard checked={false} />
          <WLCard checked={true} />
          <WLCard checked={true} />
          <WLCard checked={false} />
          <WLCard checked={true} />
          <WLCard checked={true} />
        </ScrollView>
      </View>
    );
  }
}

export default CardListScreen;

CardListScreen.propTypes = {
  navigation: PropTypes.object
};
