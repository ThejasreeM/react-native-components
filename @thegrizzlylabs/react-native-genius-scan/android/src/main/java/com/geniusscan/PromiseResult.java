package com.geniusscan;

import java.util.HashMap;

public class PromiseResult {
  public boolean isError = false;
  public String code;
  public String message;

  public String result = null;
  public HashMap<String, String> resultMap = null;

  public static PromiseResult resolve() {
    return new PromiseResult();
  }
  public static PromiseResult resolve(String result) {
    PromiseResult promiseResult = new PromiseResult();
    promiseResult.result = result;
    return promiseResult;
  }
  public static PromiseResult resolve(HashMap<String, String> resultMap) {
    PromiseResult promiseResult = new PromiseResult();
    promiseResult.resultMap = resultMap;
    return promiseResult;
  }
  public static PromiseResult reject(String errorCode, String errorMessage) {
    PromiseResult result = new PromiseResult();
    result.isError = true;
    result.code = errorCode;
    result.message = errorMessage;

    return result;
  }
}