import { StyleSheet, Dimensions } from "react-native";
import { APP_FLATLIST_NOCATEGORIES_COLOR } from "../../../colors";
const { height } = Dimensions.get("window");

const styles = StyleSheet.create({
  noCategories: {
    color: APP_FLATLIST_NOCATEGORIES_COLOR,
    fontSize: 16
  },
  noCategoriesView: {
    alignItems: "center",
    flexDirection: "column",
    height: height / 2.5,
    justifyContent: "flex-end"
  }
});
export default styles;
