import React, { Component } from "react";
import { View, Alert, Text } from "react-native";
// import QRScanner from "../../components/QRScanner";
import Icon from "react-native-vector-icons/FontAwesome5";
import * as RNLBarCode from "@yyyyu/react-native-barcode";

const Scanner = RNLBarCode.Scanner;

class ScannerScreen extends Component {
  state = {
    visible: true,
    scanEnable: true,
    scannerSize: 300,
    decoder: RNLBarCode.Decoder.Auto,
    formats: RNLBarCode.Type.Common.QR_CODE,
    torch: RNLBarCode.TorchMode.Off,
    result: ""
  };
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "QR Scanner",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      },
      headerLeft: (
        <Icon
          name={"arrow-left"}
          size={24}
          style={{ marginLeft: 20 }}
          onPress={() => {
            navigation.navigate("Home");
          }}
        />
      )
    };
  };
  handleResult = (result, error) => {
    this.setState({ scanEnable: false, content: result.content });
    if (error) {
      Alert.alert(RNLBarCode.Errors[error.code], error.message, [
        { text: "Ok" }
      ]);
    } else {
      // Alert.alert(
      //   "Success!",
      //   `format: ${RNLBarCode.Type.Common[result.format]}, content: ${
      //     result.content
      //   }`,
      //   [
      //     {
      //       text: "Ok",
      //       onPress: () => this.setState({ scanEnable: true })
      //     }
      //   ]
      // );
    }
  };

  render() {
    return (
      <View style={{ marginTop: 50, alignSelf: "center" }}>
        {this.state.visible && (
          <Scanner
            enable={this.state.scanEnable}
            decoder={this.state.decoder}
            formats={this.state.formats}
            torch={this.state.torch}
            onResult={this.handleResult}
            style={{
              width: this.state.scannerSize,
              height: this.state.scannerSize
            }}
          />
        )}
        <Text style={{ alignSelf: "center", marginTop: 20 }}>
          {this.state.content}
        </Text>
        {/* <QRScanner visible={false} scanEnable={true} scannerSize={500} /> */}
      </View>
    );
  }
}
export default ScannerScreen;
