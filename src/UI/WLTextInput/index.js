import React from "react";
import { TextInput, View, Text } from "react-native";
import PropTypes from "prop-types";
import styles from "./stylesheet";
import WLText from "../WLText";

const WLTextInput = props => {
  const { helperText } = props;
  return (
    <View style={styles.textInput}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholderTextColor="grey"
        autoCapitalize="none"
        style={[styles.input, props.style]}
        {...props}
      />
      <WLText>
        <Text style={styles.textStyle}>{helperText}</Text>
      </WLText>
    </View>
  );
};

WLTextInput.propTypes = {
  style: PropTypes.object,
  helperText: PropTypes.string
};

export default WLTextInput;
