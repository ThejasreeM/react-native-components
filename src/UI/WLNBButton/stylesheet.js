import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 25
  }
});
export default styles;
