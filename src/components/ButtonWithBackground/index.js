import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { styles as buttonStyles } from "./stylesheet";
import PropTypes from "prop-types";

export const ButtonWithBackground = props => (
  <TouchableOpacity onPress={props.onPress}>
    <View
      style={[
        buttonStyles.button,
        props.style,
        { backgroundColor: props.color }
      ]}
    >
      <Text style={buttonStyles.text}>{props.children}</Text>
    </View>
  </TouchableOpacity>
);

ButtonWithBackground.propTypes = {
  onPress: PropTypes.func,
  color: PropTypes.string,
  children: PropTypes.string,
  buttonBorderColor: PropTypes.string,
  style: PropTypes.object
};
