import React, { Component } from "react";
import { Dimensions } from "react-native";
import MapView from "react-native-maps";
import CustomMarker from "./CustomMarker";
import SuperCluster from "supercluster";

const { height, width } = Dimensions.get("window");

export default class MapWithClustering extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region: props.initialRegion,
      markers: [],
      markersOnMap: [],
      otherChildren: [],
      clusterStyle: {
        clusterTextColor: this.props.clusterTextColor,
        clusterBorderWidth: this.props.clusterBorderWidth,
        clusterTextSize: this.props.clusterTextSize,
        clusterColor: this.props.clusterColor,
        clusterBorderColor: this.props.clusterBorderColor
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    this.createMarkersOnMap(nextProps);
  }

  componentWillMount() {
    this.createMarkersOnMap(this.props);
  }

  createMarkersOnMap(propsData) {
    let markers = [];
    let otherChildren = [];

    if (propsData.children !== undefined) {
      let size = propsData.children.length;

      if (size === undefined) {
        // one marker no need for clustering
        if (propsData.children.props && propsData.children.props.coordinate) {
          markers.push({
            item: propsData.children,
            properties: { point_count: 0 },
            geometry: {
              type: "Point",
              coordinates: [
                propsData.children.props.coordinate.longitude,
                propsData.children.props.coordinate.latitude
              ]
            }
          });
        } else {
          otherChildren = propsData.children;
        }
      } else {
        let newArray = [];
        propsData.children.map(item => {
          if (item.length === 0 || item.length === undefined) {
            newArray.push(item);
          } else {
            item.map(child => {
              newArray.push(child);
            });
          }
        });
        newArray.map(item => {
          let canBeClustered = true;
          item.props.cluster === undefined
            ? (canBeClustered = true)
            : (canBeClustered = item.props.cluster);
          if (item.props && item.props.coordinate && canBeClustered) {
            markers.push({
              item: item,
              properties: { point_count: 0 },
              geometry: {
                type: "Point",
                coordinates: [
                  item.props.coordinate.longitude,
                  item.props.coordinate.latitude
                ]
              }
            });
          } else {
            otherChildren.push(item);
          }
        });
      }
      GLOBAL.superCluster = SuperCluster({
        radius: this.props.radius,
        maxZoom: 20,
        minZoom: 1
      });
      superCluster.load(markers);
      this.setState(
        {
          markers,
          otherChildren
        },
        () => {
          this.calculateClustersForMap();
        }
      );
    }
  }

  onRegionChangeComplete = region => {
    const {
      latitude,
      latitudeDelta,
      longitude,
      longitudeDelta
    } = this.state.region;
    this.props.onRegionChangeComplete(region);
    if (region.longitudeDelta <= 80) {
      if (
        Math.abs(region.latitudeDelta - latitudeDelta) > latitudeDelta / 8 ||
        Math.abs(region.longitude - longitude) >= longitudeDelta / 5 ||
        Math.abs(region.latitude - latitude) >= latitudeDelta / 5
      ) {
        this.calculateClustersForMap(region);
      }
    }
  };

  calculateBBox(region) {
    let lngD;
    if (region.longitudeDelta < 0) {
      lngD = region.longitudeDelta + 360;
    } else {
      lngD = region.longitudeDelta;
    }
    return [
      region.longitude - lngD, // westLng - min lng
      region.latitude - region.latitudeDelta, // southLat - min lat
      region.longitude + lngD, // eastLng - max lng
      region.latitude + region.latitudeDelta // northLat - max lat
    ];
  }

  getZoomLevel(bounds) {
    const WORLD_DIM = { height: height, width: width };
    const ZOOM_MAX = 20;

    function latRad(lat) {
      const sin = Math.sin((lat * Math.PI) / 180);
      const radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
      return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    function zoom(mapPx, worldPx, fraction) {
      return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
    }

    const latFraction = (latRad(bounds[3]) - latRad(bounds[1])) / Math.PI;
    const lngDiff = bounds[2] - bounds[0];
    const lngFraction = (lngDiff < 0 ? lngDiff + 360 : lngDiff) / 360;
    const latZoom = zoom(height, WORLD_DIM.height, latFraction);
    const lngZoom = zoom(width, WORLD_DIM.width, lngFraction);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
  }

  calculateClustersForMap = (region = this.state.region) => {
    let markersOnMap = [];
    let bbox = this.calculateBBox(region);
    let zoom = this.getZoomLevel(bbox);
    if (isNaN(zoom)) {
      zoom = 0;
    }
    let clusters = [];
    try {
      clusters = superCluster.getClusters(
        [bbox[0], bbox[1], bbox[2], bbox[3]],
        zoom
      );
    } catch (err) {
      console.error(err);
      clusters = [...this.state.markersOnMap];
    }
    markersOnMap = clusters.map((cluster, i) => (
      <CustomMarker
        key={i}
        clusterStyle={this.state.clusterStyle}
        onClusterPress={this.props.onClusterPress}
        customClusterMarkerDesign={this.props.customClusterMarkerDesign}
        {...cluster}
      >
        {cluster.properties.point_count === 0 ? cluster.item : null}
      </CustomMarker>
    ));
    this.setState({
      markersOnMap,
      region
    });
  };

  getLocationFromMapOnce = 0;

  sendLocationFirstTime = event => {
    //if(this.getLocationFromMapOnce == 0){
    //this.getLocationFromMapOnce += 1;
    this.props.onUserLocationChange(event);
    //}
  };

  onMapPress = () => {
    this.props.onPress();
  };

  render() {
    return (
      <MapView
        style={{
          flex: 1,
          width: width,
          height: height
          // zIndex:3000
        }}
        showsMyLocationButton={false}
        provider="google"
        onUserLocationChange={this.sendLocationFirstTime}
        showsUserLocation={true}
        loadingEnabled={true}
        toolbarEnabled={false}
        initialRegion={this.state.region}
        ref={ref => (this._root = ref)}
        onRegionChangeComplete={this.onRegionChangeComplete}
        onPress={this.onMapPress}
      >
        {this.state.markersOnMap}
        {this.state.otherChildren}
      </MapView>
    );
  }
}
MapWithClustering.defaultProps = {
  clusterColor: "#F5F5F5",
  clusterTextColor: "#FF5252",
  clusterBorderColor: "#FF5252",
  clusterBorderWidth: 1,
  clusterTextSize: 2.5
};
