
#import "RNGeniusScan.h"
#import "GSSDK-UI/Scan.h"
#import "GSSDK-UI/ScanNavigationController.h"

#import <React/RCTConvert.h>
#import <React/RCTUtils.h>
#import <GSSDK/GSSDK.h>

@interface RNGeniusScan()
- (void)presentViewController:(UIViewController *)viewController rejecter:(RCTPromiseRejectBlock)reject;
@end

@implementation RNGeniusScan

- (dispatch_queue_t)methodQueue
{
  return dispatch_get_main_queue();
}

- (void)presentViewController:(UIViewController *)viewController rejecter:(RCTPromiseRejectBlock)reject {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *root = RCTPresentedViewController();
        @try {
            [root presentViewController:viewController animated:YES completion:nil];
        }
        @catch (NSException *exception) {
            reject(@"error", exception.reason, nil);
        }
    });
}

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(setLicenceKey:(NSString *)licenceKey resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    BOOL validLicence = [GSK initWithLicenseKey:licenceKey];

    if (validLicence) {
        resolve(nil);
    } else {
        reject(@"invalid_licence", @"License key is not valid or has expired.", nil);
    }
}

RCT_EXPORT_METHOD(scanImage:(NSString *)originalImageUri
                  scanOptions:(NSDictionary *)scanOptions
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject
) {
    UIViewController *viewController = [ScanNavigationController scanNavigationControllerFromImageURL:[NSURL URLWithString:originalImageUri] resolver:resolve rejecter:reject scanOptions:scanOptions];
    [self presentViewController:viewController rejecter:reject];

}

RCT_EXPORT_METHOD(scanCamera:(NSDictionary *)scanOptions
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject
) {
    UIViewController *viewController = [ScanNavigationController scanNavigationControllerFromCamera:resolve rejecter:reject scanOptions:scanOptions];

    [self presentViewController:viewController rejecter:reject];
}

RCT_EXPORT_METHOD(generatePDF:(NSString *)title
                  imageFileUris:(NSArray *)imagePaths
                  pdfOptions:(NSDictionary *)pdfOptions
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject
) {
    @try {
        NSString *password = pdfOptions[@"password"];
        // First we generate a list of pages.
        NSMutableArray *pages = [NSMutableArray array];

        [imagePaths enumerateObjectsUsingBlock:^(NSString *filePath, NSUInteger idx, BOOL * _Nonnull stop) {
            /// For each page, we specify the document and a size in inches.
            GSKPDFPage *page = [[GSKPDFPage alloc] initWithFilePath:filePath inchesSize:[[GSKPDFSize alloc] initWithWidth:8.27 height:0]];
            [pages addObject:page];
        }];

        /// We then create a GSKPDFDocument which holds the general information about the PDF document to generate
        GSKPDFDocument *document = [[GSKPDFDocument alloc] initWithTitle:title password:password keywords:nil pages:pages];

        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];

        NSString *outputFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", title]];

        /// Last, we instantiate a GSKPDFGenerator and generate the document
        GSKPDFGenerator *generator = [GSKPDFGenerator createWithDocument:document];
        [generator generatePDF:outputFilePath];

        resolve(outputFilePath);
    }
    @catch (NSException *exception) {
        reject(@"error", exception.reason, nil);
    }
}

@end
