import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    flex: 1,
    height,
    width
  },
  mapMarker: { height: 40, width: 40 }
});

export default styles;
