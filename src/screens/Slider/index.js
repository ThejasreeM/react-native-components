/* eslint-disable react-native/no-unused-styles */
/* eslint-disable react-native/no-color-literals */
import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import CustomSlider from "../../components/Slider";
import Icon from "react-native-vector-icons/FontAwesome5";
import { SLIDER_HIGH, SLIDER_LOW, SLIDER_MEDIUM } from "../../../colors";

class SliderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slider: {
        value: null,
        minValue: 0,
        maxValue: 100,
        sliderValue: "Low",
        sliderColor: null
      }
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Slider",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      },
      headerLeft: (
        <Icon
          name={"arrow-left"}
          size={24}
          style={{ marginLeft: 20 }}
          onPress={() => {
            navigation.navigate("Home");
          }}
        />
      )
    };
  };
  // slider move function for slider component
  sliderMove = (val, col) => {
    let stateCopy = { ...this.state.slider };
    stateCopy.value = val;
    stateCopy.sliderColor = col;
    this.setState({ slider: stateCopy });
    let slide = val;
    let slideCol = col;
    if (slide >= 0 && slide <= 25) {
      slide = 0;
      slideCol = SLIDER_LOW;
      stateCopy.sliderValue = "Low";
      this.setState({ slider: stateCopy });
    }
    if (slide >= 26 && slide <= 74) {
      slide = 50;
      slideCol = SLIDER_MEDIUM;
      stateCopy.sliderValue = "Medium";
      this.setState({ slider: stateCopy });
    }
    if (slide >= 75 && slide <= 100) {
      slide = 100;
      slideCol = SLIDER_HIGH;
      stateCopy.sliderValue = "High";
      this.setState({ slider: stateCopy });
    }
    stateCopy.value = slide;
    stateCopy.sliderColor = slideCol;
    this.setState({ slider: stateCopy });
  };
  render() {
    return (
      <View style={{ flex: 1, marginTop: 30 }}>
        {/* <Text style={styles.text}>Spice</Text>
        <Text style={styles.subtext}>Set your overall spice level</Text> */}

        <CustomSlider
          sliderProps={this.state.slider}
          sliderMove={this.sliderMove.bind(this)}
        />
      </View>
    );
  }
}

export default SliderScreen;

SliderScreen.propTypes = {
  navigation: PropTypes.object
};
export const styles = StyleSheet.create({
  subtext: {
    color: "black",
    fontSize: 13,
    paddingBottom: 20,
    paddingLeft: 20,
    position: "relative"
  },

  text: {
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
    paddingBottom: 5,
    paddingLeft: 20,
    paddingTop: 10,
    position: "relative"
  }
});
