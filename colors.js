export const BUTTON_BORDER_COLOR = "#A52A2A";

export const TEXT_COLOR = "#FFF";

export const MODAL_BACKGROUND_COLOR = "#FFF";

export const APP_FLATLIST_NOCATEGORIES_COLOR = "#666666";

export const APP_PRIMARY_COLOR = "#FFA500";

export const COLOR_ALERT = "#ff0000";

export const COLOR_GREY = "#D3D3D3";

export const APP_PRIMARY_BORDER_COLOR = "#808080";

export const APP_HEADER_TEXT_COLOR = "#FFFFFF";

export const COLOR_DARK_GREY = "#696969";

export const APP_SECONDARY_BACKGROUND_COLOR = "#FFFFFF";

export const TRANSPARENT = "transparent";

export const BACKGROUND_COLOR_ORANGE = "orange";

export const SLIDER_THUMB = "#696969";
export const SLIDER_LOW = "#FFD700";

export const SLIDER_MEDIUM = "#FFA500";

export const SLIDER_HIGH = "#FF4500";
