export const MarkersData = [
  {
    title: "Marker1",
    id: "1",
    lat: 17.4948,
    lng: 78.3996
  },
  {
    title: "Marker2",
    id: "2",
    lat: 17.4435,
    lng: 78.3772
  },
  {
    title: "Marker3",
    id: "3",
    lat: 17.4486,
    lng: 78.3908
  },
  {
    title: "Marker4",
    id: "4",
    lat: 15.3173,
    lng: 75.7139
  },
  {
    title: "Marker5",
    id: "5",
    lat: 13.0827,
    lng: 80.2707
  },
  {
    title: "Marker6",
    id: "6",
    lat: 35.8617,
    lng: 104.1954
  },
  {
    title: "Marker7",
    id: "7",
    lat: 56.1304,
    lng: 106.3468
  },
  {
    title: "Marker8",
    id: "8",
    lat: 14.235,
    lng: 51.9253
  },
  {
    title: "Marker9",
    id: "9",
    lat: 37.0902,
    lng: 95.7129
  },
  {
    title: "Marker10",
    id: "10",
    lat: 61.524,
    lng: 105.3188
  },
  {
    title: "Marker11",
    id: "11",
    lat: 55.3781,
    lng: 3.436
  },
  {
    title: "Marker12",
    id: "12",
    lat: 51.1657,
    lng: 10.4515
  },
  {
    title: "Marker13",
    id: "13",
    lat: 0.7893,
    lng: 113.9213
  }
];
