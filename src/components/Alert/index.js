/* eslint-disable no-console */
import { Alert } from "react-native";

const alert = props => {
  //console.log(props);
  return Alert.alert(
    props,
    "",
    [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );
};

export default alert;
