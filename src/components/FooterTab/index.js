/* eslint-disable react/prop-types */
/* eslint-disable react-native/no-raw-text */
import React from "react";
import { Footer, FooterTab } from "native-base";
import WLNBButton from "../../UI/WLNBButton";
import WLNBText from "../../UI/WLNBText";
import WLNBIcon from "../../UI/WLNBIcon";

const CustomFooter = () => {
  return (
    <Footer>
      <FooterTab>
        <WLNBButton active>
          <WLNBText>ALERT</WLNBText>
          <WLNBIcon name="ios-alert" />
        </WLNBButton>
        <WLNBButton>
          <WLNBText>APPS</WLNBText>
          <WLNBIcon name="ios-apps" />
        </WLNBButton>
      </FooterTab>
    </Footer>
  );
};

export default CustomFooter;
