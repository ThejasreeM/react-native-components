/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  card: {
    backgroundColor: "#fff",
    borderRadius: 5,
    flexDirection: "row",
    height: "20%",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10
  },
  centeredTextView: {
    flexDirection: "column",
    marginRight: 10,
    width: "40%"
  },
  container: {
    backgroundColor: "grey",
    flex: 1
  },
  icon: {
    color: "blue",
    fontSize: 30,
    fontWeight: "bold",
    justifyContent: "flex-end"
  },
  iconView: {
    alignSelf: "center",
    marginLeft: 40,
    marginRight: 20,
    marginTop: 40,
    width: "14%"
  },
  image: {
    borderRadius: 20,
    height: "60%",
    marginLeft: 30,
    marginRight: 10,
    marginTop: 20,
    width: "25%"
  },
  mainText: {
    alignSelf: "center",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 40
  },
  text: {
    alignSelf: "center",
    fontSize: 15,
    marginTop: 5
  }
});
