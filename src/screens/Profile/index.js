import React, { Component } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import ProfileAvtar from "../../components/profilePicAvtar";
import Icon from "react-native-vector-icons/FontAwesome5";

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Profile",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      },
      headerLeft: (
        <Icon
          name={"arrow-left"}
          size={24}
          style={{ marginLeft: 20 }}
          onPress={() => {
            navigation.navigate("Home");
          }}
        />
      )
    };
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ProfileAvtar />
      </View>
    );
  }
}

export default ProfileScreen;

ProfileScreen.propTypes = {
  navigation: PropTypes.object
};
