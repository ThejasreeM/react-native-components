
import { NativeModules } from 'react-native';

const { RNGeniusScan } = NativeModules;

// Mapping to make options optional
export default {
  scanCamera: (scanOptions = {}) => RNGeniusScan.scanCamera(scanOptions),
  scanImage: (imageFileUri, scanOptions = {}) => RNGeniusScan.scanImage(imageFileUri, scanOptions),
  setLicenceKey: RNGeniusScan.setLicenceKey,
  generatePDF: (title, imageFileUris, pdfOptions = {}) => RNGeniusScan.generatePDF(title, imageFileUris, pdfOptions),
  ENHANCEMENT_NONE: 'none',
  ENHANCEMENT_BW: 'bw',
  ENHANCEMENT_PHOTO: 'color',
  ENHANCEMENT_COLOR: 'whiteboard',
};
