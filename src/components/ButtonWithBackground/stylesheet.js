import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
import { BUTTON_BORDER_COLOR, TEXT_COLOR } from "../../../colors";
export const styles = StyleSheet.create({
  button: {
    alignSelf: "center",
    borderColor: BUTTON_BORDER_COLOR,
    borderRadius: 8,
    borderWidth: 4,
    height: height / 12,
    margin: 5,
    padding: 10,
    width: width / 2
  },
  text: {
    //alignSelf: "center",
    color: TEXT_COLOR,
    fontSize: 17,
    fontWeight: "bold",
    paddingBottom: 20
  }
});
