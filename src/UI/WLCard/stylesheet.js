/* eslint-disable react-native/sort-styles */
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  // eslint-disable-next-line react-native/no-color-literals
  cardView: {
    backgroundColor: "#fff",
    borderRadius: 5,
    flexDirection: "row",
    height: "10%",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10
  },
  checkImage: {
    height: 24,
    width: 24
  },
  centeredTextView: {
    flexDirection: "column",
    marginRight: 10,
    width: "40%"
  },
  checkedView: {
    marginRight: 10,
    marginTop: 15,
    width: "8%"
  },
  image: {
    borderRadius: 12,
    height: "53%",
    margin: 20,
    width: "13%"
  },
  input: {
    marginLeft: 20,
    marginTop: 20,
    padding: 5,
    width: "90%"
  },
  itemsCount: {
    fontSize: 16,
    fontWeight: "bold",
    marginRight: 10
  },
  itemCountView: {
    flexDirection: "row",
    marginRight: 5,
    marginTop: 30,
    width: "14%"
  },
  itemsText: {
    fontSize: 14
  },
  mainText: {
    fontSize: 15,
    fontWeight: "bold",
    marginTop: 15
  },
  text: {
    fontSize: 15,
    marginTop: 5
  }
});
