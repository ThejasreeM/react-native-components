/* eslint-disable react-native/no-raw-text */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Container, Content } from "native-base";
import { styles as cartStyles } from "./stylesheet";
import CustomFooter from "../../components/FooterTab";
import Icon from "react-native-vector-icons/FontAwesome5";

class CartScreen extends Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Footer",
      headerTintColor: "royalblue",
      headerStyle: {
        backgroundColor: "orange"
      },
      headerLeft: (
        <Icon
          name={"arrow-left"}
          size={24}
          style={{ marginLeft: 20 }}
          onPress={() => {
            navigation.navigate("Home");
          }}
        />
      )
    };
  };
  onPressIcon = () => {
    this.props.navigation.navigate("Login");
  };
  alert = () => {
    this.alert("Hi");
  };
  render() {
    return (
      <Container style={cartStyles.container}>
        {/* <WLMainCard navigation={this.props.navigation} /> */}
        {/* <Text>Custom Footer</Text> */}
        <Content></Content>
        <CustomFooter />
      </Container>
    );
  }
}

export default CartScreen;

CartScreen.propTypes = {
  navigation: PropTypes.object
};
